LATEX=latexmk --pdf -interaction=batchmode -silent
NAME=main
FINALPDF=$(NAME).pdf
AUX_SOURCE=$(NAME).aux
MAIN_SOURCE=$(NAME).tex
SOURCE=*.tex  bibliography/*.bib toinclude.tex  # sections/*.tex # biblatex.cfg
TEMPFILES=*.aux sections/*.aux *.out *.log *.toc *.nav *.snm  # *~

# output color:
#COLOR="\033["
WHITE="\033[38;1m"
GREEN="\033[32;1m"
CLOSE="\033[0m"

# prefixes:
WORK=$(GREEN)"::"$(CLOSE)
INFO=$(GREEN)" >"$(CLOSE)


all: $(FINALPDF)
	@echo $(INFO) $(WHITE)"Compiled"$(CLOSE)

$(FINALPDF): $(MAIN_SOURCE) $(SOURCE)
	@echo $(WORK) $(WHITE)"Compiling" $@ $(CLOSE); $(LATEX) $(MAIN_SOURCE) 2>&1 > /dev/null
	@echo -n "\t"$(GREEN)[$(CLOSE)$(WHITE)"Done"$(CLOSE)$(GREEN)]$(CLOSE)

# $(AUX_SOURCE): $(SOURCE)
# 	@echo -n $(WORK) $(WHITE)"Compiling" $@ $(CLOSE); $(LATEX) $(MAIN_TEX_SOURCE) 2>&1 > /dev/null
# 	@echo "\t"$(GREEN)[$(CLOSE)$(WHITE)"Done"$(CLOSE)$(GREEN)]$(CLOSE)

clean:
	@echo $(WORK) $(WHITE)"Clean useless files"$(CLOSE)
#	@rm -f $(FINALPDF) $(TEMPFILES)
	@latexmk -c $(MAIN_SOURCE)

erase:
	@echo $(WORK) $(WHITE)"Erase useless & aux files"$(CLOSE)
	@rm -f $(FINALPDF) $(TEMPFILES) $(AUX_SOURCE)
	@latexmk -C $(MAIN_SOURCE)

open: $(FINALPDF)
	@echo $(INFO) $(WHITE)"Apro $(FINALPDF)"$(CLOSE)
	@xdg-open $(FINALPDF)

word-count: $(MAIN_SOURCE) $(SOURCE)
	@texcount -1 -sum -inc $(MAIN_SOURCE)
